# My Vim configuration

## How to install
* `git clone git@git.framasoft.org:hamadr/vim_config.git ~/.vim_config`
* add this line to your .vimrc : `source ~/.vim_config/.vimrc`
* open Vim `:PlugInstall`
* reboot Vim to apply colorscheme

## Plugins
* Nerdtree
* Ctrl-p
* Fugitive
* Vim-airline with luna theme
* BufExplorer
