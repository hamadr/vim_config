set runtimepath+=~/.vim_config

set mouse=a
set nu
set history=500
set autoread
let mapleader = ","
let g:mapleader = ","
set ruler
set cmdheight=2
set backspace=eol,start,indent

"This unsets the "last search pattern" register by hitting return
nnoremap <leader><CR> :noh<CR>

highlight ColorColumn ctermbg=0
set colorcolumn=120

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

set smarttab

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

syntax enable
set nobackup
set nowb
set noswapfile

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

set laststatus=2

if ! has("gui_running")
    set t_Co=256
endif
set background=dark
colorscheme luna-term

" Underline current search pattern while doing :s///c
hi IncSearch ctermbg=228 ctermfg=black cterm=underline guifg=#272822 guibg=#e6db74 gui=NONE
hi Search ctermbg=228 ctermfg=black guifg=NONE guibg=NONE

source ~/.vim_config/plugins.vimrc
